Reading 05
==========
1.
i. 	The purpose of the import sys, is to import the module labeled sys. This module provides a number of functions and variables that can be used to manipulate differnt parts of the Python runtime environment. Some notable functions are argv which contains arguments passed to the script.

ii. 	This declares the variable arg and loops through the sys.argv[1:] range. This is will output every element that was passed to the script not including itself. Thus it will start at the 1 position or the second element.

iii. This prevents a new line from being emitted. The comma is to show that the string will be placed on the next line without a new line in between the two.

2.
i. The while loop works very similarly to how we have been parsing command line arguments in shell scripting. Rather than using shift to advance to the next element, we are using the pop function. This deletes the element and returns it to be saved to the variable args. If that variable is equal to -E, then it will add the $ to ENDING to later be appended. If other commands are passed it goes to the usage test. This while loop continues until the length of the string is 0, the command doesn't start with -, or the length is less than 1.

ii.	If the length of the elements of args is ==0 after the while statement, than add a '-' to the end of the list. The other one then checks to see if the variable path is equal to '-'. If it is then the stream of numbers it reads from is from standard in (user inputs numbers). If not, it opens the stream from the file with the variable name path is going to.


iii. rstrip() returns a copy of the string in which all chars (default is whitespace)  have been stripped from the end of the string.


