Reading 08 - Grading
========================
**Score**: 2.75/4

**Grader**: Mimi Chen


Deductions
------------------
(-0.25) Q1.3 13 bytes 

		8 bytes for pointer + 5 chars (need to include NUL)

(-0.25) Q1.6 24 bytes 

		8 bytes for pointer + 16 bytes for struct

(-0.25) Q1.7 168 bytes

		8 bytes for pointer + 10 point structs

(-0.25) Q1.8 88 bytes

		8 bytes for pointer + 10 pointers to structs

(-0.25) duplicates.c: Incorrect allocation of randoms

		 int *randoms = malloc(n * sizeof(int));



Comments
------------------
