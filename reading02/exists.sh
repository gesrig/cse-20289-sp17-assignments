#!/bin/sh

if [ $# == 0 ]; then 
	echo "ERROR: THERE NEEDS TO BE AN ARGUMENT"
	exit 1
fi


for value  in $@
do
	if [ -e "$value" ]; then
        	echo "$value exists!"
	else echo "$value does not exist!"
		exit 1
	fi
done

exit 0

