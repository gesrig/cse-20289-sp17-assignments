Reading 02
==========
1. Run the script even though not executable.
/bin/sh exists.sh   

2. A: Type in chmod 755 exists.sh to make it exectuable. Than type in ./exists.

3. Once it is executable you can run ./exists.sh

4. This tells Unix that the file is to be executed by /bin/sh. This is the location of the bourne shell on the Unix system.

5. This outputs that "exists.sh exists."

6. $1 is the first additional parameter in which this script was called with. This is the next command/word after the executable is run.

7. This verifies if the first paraemter after the executable is a file in the current directory

8. This script checks whether certain files are in or are not in this specified directory









