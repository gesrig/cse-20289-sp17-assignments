Reading 09
==========

1.

	i. The size of s is 16 bytes. The sizeof u is 8 bytes.
	ii. The primary difference between a union and a struct is that more memory is allocated to structs than to unions. The memory for a struct is the sum of memory size of all members. for a union, it is the memory required to hold the largest element in the union. Moreover, only one of a union's members can be accessed at a time.

2.
	i. This outputs "fate is inexorable!"
	ii. When running the DUMP_BITS macro in the program for v0, v1, and v2, I get "00100000 01110011 01101001 00100000 01100101 01110100 01100001 01100110
	 01100010 01100001 01110010 01101111 01111000 01100101 01101110 01101001
	  00000000 00000000 00000000 00000000 00001010 00100001 01100101 01101100" which is the sttement above in ascii in reverse. This is because when outputting with the loop, it is decrementing and not incrementing. The program is able to compute a final message based on taking advantage of how ascii values are stored and used. For example, the word fate has its characters stored in the same binary for a given integers. So when the word is printed, it takes the corresponding char value to that binary and prints that to stdout.
	iii. This programs demonstrates a lot about how types, memory and data is represented in C. All data values are stored in binary which are simple 1s and 0s to tell the compiler what to output to the screen. Types are simply ways to read the same binary number. Memory is simply a collection of these 1s and 0s stored. Data can be represented in many different ways in C but at the basis of it, is just how these 1s and 0s are interpreted. When we cast values in C, we are telling the computer to analyze a chunk of memory in an alternative way.


3.
	i. A collision occurs when two different inputs to a function produce the same output. 
	ii. In a has table that uses separate chaining, all keys that map to the same hash value are kept in a list. In other words, it hangs an additional data structure off of the buckets. The bucket array becomes an array of link list. Then we go to the bucket to compare keys and if the linked list is used the hash never fills.
	iii. Using seaparate chaining, the data structrue can grow without bounds.Open addressing does not introduce a new structure. If a collision occurs then we look for availability in the next spot generated. Linear propbing tries to insert item (k,e) into a bucket X[i]. If it is full then you try the bucket X[(i+1) mod N]. Other methods that can be used are quadratic probing and Double hashing.

4. 
	i.

	0	|	
	1	|
	2	|	2 --> 72
	3	|	3 --> 13
	4	| 	14
	5	|
	6	|	56
	7	|	7
	8	|	78 --> 68
	9	|	79

	ii.

	0   |	68
	1   |	14
	2   |   2
	3   |   3
	4   |   72
	5   |	13
	6   |  	56
	7   |   7
	8   |   78
	9   |	79
