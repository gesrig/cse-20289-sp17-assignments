Homework 01
===========
ACTIVITY 1:
1.
a) In my Public directory, the system authuser can read files, lookup (examine and access subdirectories), and lock files. This is the same with nd_campus. In my Home, nd_campus can only lookup as well as the authuser. In private only the system adminstrators and I have full access.

b) In my Private data the admistrators are given full permission as well as me (rlidwka). In Public, nd_campus and authuser have rlk. However both adminstrators and I still have full access.

2. 
a)Permission denied
b)The Unix permissions would have allowed myself to access it because when typing ls -l it gives me drwxrwxrwx. Because it failed, the ACLs must take precedence

ACTIVITY 2:
| Command                             | Elapsed Time  |
|-------------------------------------|---------------|
| cp -r /usr/share/pixmaps/ images    | 1.38  seconds |
| mv images pixmaps                   | 0.005 seconds |
| mv pixmaps/ /tmp/gesrig-pixmaps     | 0.820 seconds |
| rm /tmp/gesrig-pixmaps              | 0.002 seconds |

1. the second takes significantly less time because rather than simply changing the text that labels the file, the contents of the file have to be copied to a different location

2. It is much easier to delete all the contents from a memory perspective rather than to copy them in a different location.

ACTIVITY 3:
1. bc < math.txt
2. bc <  math.txt > results.txt 2>&1
3. bc <  math.txt > results.txt 2> /dev/null
4. bc <  math.txt | cat
	This is less efficient because you have to process two commands separately rather that at the same time.

ACTIVITY 4:
1. 49. Command used: grep -c "sbin/nologin" passwd
2. 2. Command : who -q
3. Command: du -h 2>/dev/null | sort -h
	7.0M	./selinux/targeted/policy
	7.6M	./selinux
	7.6M	./selinux/targeted
	8.4M	./gconf
	36M	.
4. Command: ps | grep -c bash
	1 user

ACTIVITY 5:
1.
a) kill or cntrl c and many others
b) cntrl z to stop the process. Then using ps to get the PID number you can press kill -KILL PID to get rid of the process.

2.  
a) pgrep -u gesrig TROLL | xargs kill -KILL

	This find the PID associated with gesrig. This PID is then pipelined in using xargs and sends the KILL signal to end it

b) pkill -KILL TROLL
	This tries to kill many processes and if it isn't allowed it will output a list of other PIDs where this is not permitted. pkill works by using a partial name to find the PID number. That name is TROLL

3. 
Certain signals like kill -HUP change the taunts. However if you pass in something like kill -USR1, you get a numbers and letters streaming down the page. 





 

 
