GRADING - Exam 02
=================

- Identification:   2.75
- Web Scraping:     4
- Generators:       5.75
- Concurrency:      5.25
- Translation:      5.75
- Total:	    23.5

Comments
--------

- 0.25  Translation Q1 - Wrong path, output not lowercase
