#!/usr/bin/env python2.7

import re

#cat /etc/passwd
with open('etc/passwd', 'r') as f:
	text = f.readlines()

#cut -d : f
fields = [line.split(':')[4] for line in text]

regex = r'[uU]ser'

res = [ sol for sol in fields if re.search(regex, sol) ]

for value in res:
	value = value.lower()

res = sorted(res)

for x in res:
	print x


