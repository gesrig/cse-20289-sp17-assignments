#!/usr/bin/env python2.7

import os

# use the ps aux command and pipe it into a readeable thing

psaux = os.popen('ps aux' , 'r')

#splice out the first column needs to be a string sort because first column

names = [str(line.split()[0])  for line in psaux.read().rstrip().split('\n')]

#sort, uniq, and wc -l

uniq = len(sorted(set(names)))

print uniq
