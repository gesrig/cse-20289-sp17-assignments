/* sum.c */

#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_NUMBERS (1<<10)

/* Functions */

size_t read_numbers(FILE *stream, int numbers[], size_t n) {
    size_t i = 0;

    while (i < n && scanf("%d", &numbers[i]) != EOF) {
        i++;
    }

    return i;
}


/* Main Execution */

int main(int argc, char *argv[]) {
    int numbers[MAX_NUMBERS];
    int  it, d, z;
    size_t nsize;

    nsize = read_numbers(stdin, numbers, MAX_NUMBERS);

    for ( it = 1; it < nsize; it++) {
	d = it;

	while ( d>0 && numbers[d] < numbers[d-1]) {
	     z = numbers[d];
	     numbers[d] = numbers[d-1];
	     numbers[d-1] = z;

	     d--;
	}
    } 	
    if (nsize ==0) {
	printf ("\n");
    }
    else {  
        for ( int l = 0; l < nsize-1; l++) {
	    printf("%d " , numbers[l]);
        }
        printf("%d\n", numbers[nsize-1]);
    }
   // printf ( "\b");

    return EXIT_SUCCESS;
}
