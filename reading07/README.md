Reading 07
==========
ACTIVITY 1

1. The overall goal of the read numbers function is to read the numbers that are inputted and return the amount of numbers as well as store an array of ints with the given numbers inside it. This line scans the input stream for integers, skipping white space, and stores them into the memory block of numbers, incrementing by one each time. The ampersand becomes a pointer to the memory block numbers. This continues until i is less than the max capacity or the EOF occurs. EOF is either cntrl d from command line or the end of an echo statement.

2. yes this would still work. Because numbers is an array, one could use this function which would return the length of the array. Despite i being a unsigned int, it would still go until it was less than that value.


ACTIVITY 2

1. This version of cat parses commnad line arguments is similar and different in a few ways. The py file takes an array of the elements while the c file reads them in as an array of cstrings. Where as cat.py starts at the first, cat.c declares the first element as the PROGRAM_NAME. In cat.c, the variable argind is incremented and thus is how it is moved through the array, in cat.py, the values are popped off. To get the actual value, the py file pops a value and uses a case statment, where the c version runs a case with a pointer's value to a certain memory block in the argv array. In the c file, argc is used as the number of elements and thus if argind is ever the same, the while will break.

2. While the value of argind is less than the length of the array with the values, a character pointer is assigned the value of the certain element in the argv array. Strcmp works by comparing the path variable to the -, and returns 0 if they are not the same. Thus if they are, it calls the cat_stream function on the stdin. If they are the same, then it calls in to that variable.

3. The cat_stream function works by declaring a character array with size BUFSIZ, which is declared in each system and min 256 to make I/O more efficient. fgets works by reading the next line of text into a string at least BUFSIZ-1 times. fputs works by reading the c string pointed to by buffer to stdout. Thus in short wile it can read in lines it is emitting them to the stdout

4. The cat_file functions by opening up the FILE pointer fs to the name of the file. The if statement works by checking if fs is NULL, or the file does not exist. If this is the case it prints the PROGRAM name, the path and the streerror to the file called stderr. If it is not NULL it calls the cat_stream file and writes the contents and then closes it. In the if statement, the strerror interprets the value of errnum, and generates a string with a message that descibes the error condition as if set to errno by a function of the library.
