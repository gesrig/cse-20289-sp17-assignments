#!/bin/sh

state="Indiana"
if [ $1 = '-h' ] ; then
  echo "Usage: zipcode.sh"
  echo " "
  echo "  -c	CITY	Which city to search"
  echo "  -f	FORMAT  Which format (text,csv)"
  echo "  -s	STATE   Which state to search (Indiana)"
  echo "   "
  echo "If not City is specified, then all the zip codes for the State are displayed" 
  exit 0;
fi

if [ $# == 0 ] ; then
	curl -s http://www.zipcodestogo.com/Indiana/  | grep -Eo '>[0-9]{5}<' | grep -Eo '[0-9]{5}' |  sort | uniq 
	exit 0
fi


while [ $# -gt 0 ] ; do

	case "$1" in 
	
	  -s) 	
		shift	
		state=$(echo $1 | sed 's_ _%20_') 
		shift	
		;;
	  -c)  
		shift
		city=$1
		shift
		;;

	  -f) 
		shift 
		if [ $1 == 'csv' ] ; then
		  value="paste -d, -s"
		  curl -s http://www.zipcodestogo.com/$state/ | grep -E "/$city/" | grep -Eo '>[0-9]{5}<' | grep -Eo '[0-9]{5}' | sort | uniq | $value;
		  exit 0;		
		fi
		
		shift 
		;;

	   *)   
	
		break
	esac

done

curl -s http://www.zipcodestogo.com/$state/ | grep -E "/$city/" | grep -Eo '>[0-9]{5}<' | grep -Eo '[0-9]{5}' | sort | uniq 
