Homework 03
===========
Activity 1:

1. I parsed the command line arguments using $# and seeing if there was another number or if the default had to bee used. The amount it was shifted by was set automatically to 13 and only changed if $1 was equal to a number

2. I constructed the source set using two variables, SouceL and SourceU/. These variables were just a string of the alphabet in lower and upper case respectively. 

3. The Target set was constructed by using the cut command on each of the variable at the specirfied amount. After the cut was made, the two halves were stored in new variables and then combined in reverse order to create the target set.

4. I then used the tr command first for the lower case where everything in the source set was changed to the target. Then I piped this output into another tr command that had all the upper souce changed to the upper target

Activity 2:
1. I parsed the command line arguments with a while loop. Each time the $1 would get to a flag I would then shift to enable the case structure to properly identify its next target

2. I removed the comments using the sed command. $ sed  "s|$delim.*$||" | sed 's_\s*$__' | sed '/^$/d' enabled me use the variable delim to identify what I wanted to cut out. Then using the \s I took out white space as well as the space folloing it. The last sed command took out any other whitespace available.

3. I removed empty lines by using the second sed command described above. $ sed '/^$/d' allowed me to remove from the beginning to the end using the anchor commands.

4. The command line options affected my operations due to the nature of a case. I needed to declare a true or false statement or else the W flag would not be able to recover space already taken away with the d flag. This is the reason that the command is at the end of the file.

Activity 3:

1. I parsed the command line arguments by using a while loop that would go until the #$ was 0. By using the shift command I was able to continue through all the command line arguments and still call the $1 for the flags as well for the case.

2. I extracted the zipcodes by first using the grep ">[0-9]{5}<" and then grepping again but only for [0-9]{5}. This insured that no other numbers greater than five digits were being taken. 

3. I filtered the State and city using a case based on each flag. The state variable was set as default to Indiana, but would take in the string after the -s flag. Moreover, I used the sed command with a %20 if there was a space between the state to make sure it would still run. City was simply done by taking in the next argument of the flag.

4. I handled the csv format by using a paste command $ paste -d, -s which was piped at the end of my command. For handling text, this was done without any command because the standard output defaults it to this
