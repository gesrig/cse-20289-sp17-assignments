#!/bin/sh



SourceL="abcdefghijklmnopqrstuvwxyz";
SourceU="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
amount=$1;

if [ $# == 0 ]; then
	amount=13;
fi

if [ $1 == '-h' ]; then
	echo "Usage: caesar.sh [rotation]" 
	echo " "
	echo "This program will read from stdin and rotate (shift right) the text by the specified rotation. If none is specified then the default value is 13."
	exit 0;
fi

TargetL1=`echo $SourceL | cut -c 1-$amount`;
TargetL2=`echo $SourceL | cut -c $(($amount+1))-26`;
TargetLow=$TargetL2$TargetL1;

TargetU1=`echo $SourceU | cut -c 1-$amount`;
TargetU2=`echo $SourceU | cut -c $(($amount+1))-26`;
TargetUpp=$TargetU2$TargetU1;



#tr '[$SourceL $TargetLow]' '[$SourceU$TargetUpp]' ;  
#tr $SourceU $TargetUpp ;

tr $SourceL $TargetLow |tr  $SourceU $TargetUpp ;


