#!/bin/sh

delim='#'
justdflag=1;

if [ $1 = '-h' ] ; then
  echo "Usage: zipcode.sh"
  echo " "
  echo "  -d    CITY    Use this as the comment delimiter"
  echo "  -W    FORMAT  Don't strip empty lines"
  exit 0;
fi

while [ $# -gt 0 ] ; do

        case "$1" in

          -d)
                shift
                delim=$1;  
                shift
                ;;
          -W)
                justdflag=0; 
               
                shift
                ;;

           *)

                break
        esac

done

if [ $justdflag == 1 ] ; then
	sed  "s|$delim.*$||" | sed 's_\s*$__' | sed '/^$/d'
else
	sed -e "s|$delim.*$||" | sed 's_\s*$__'
fi

