#!/usr/bin/env python2.7

import sys

print ' '.join( filter(lambda x2: int(x2) %2 == 0, map(lambda x1: x1.strip(), sys.stdin)))

