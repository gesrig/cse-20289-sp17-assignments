Reading 06
==========
A. The problem that MapReduce is trying to solve is processingg large data sets in an efficient manner. MapReduce understands that most computations are conceptually straightforward, but the computations must be distributed across large amounts of machines dealing with complex code. MapReduce allows us to express simple computations and hides messy details of parallelization.

B. The Three Phases
i. Map Phase: In this phase, data in each split is passed to a mapping function to produce output values.

ii. Shuffle: Nodes redistribute data based on the output of the map function. This makes it so all data belonging to one key is located on the same node.

iii. Reduce phase: In this phase, output values from the intermediate files are aggregated. This takes values from the intermediate phase and returns a single output value. This summarizes the complete dataset
