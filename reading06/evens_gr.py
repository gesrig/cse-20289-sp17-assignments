#!/usr/bin/env python2.7

import sys

def evens(stream):
    for i in stream:
	i = i.strip()
	if int(i) %2 == 0:
		yield i

print ' '.join(evens(sys.stdin)),
