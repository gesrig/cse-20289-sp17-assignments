#!/bin/sh
descend=0;
AMOUNT=5;

usage() {
    cat <<EOF
Usage: $(basename $0)
	Usage: rank.sh [-n N -D]
		-n N	Returns N items (default is 5)
		-D	Rank in descending order
EOF
    exit $1
}

while [ $# -gt 0 ]; do
    case $1 in
    -n)	AMOUNT=$2 shift;;
    -D)	descend=1;;
    *)	usage 0;;
    esac
    shift
done

if [ $descend = 1 ]; then
   sort -nr | head -n $AMOUNT
else
   sort -n | head -n $AMOUNT
fi

