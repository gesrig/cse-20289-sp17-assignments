GRADING - Exam 01
=================

- Commands:         2
- Short Answers:
    - Files:        2.5 
    - Processes:    3 
    - Pipelines:    3
- Debugging:        3.25
- Code Evaluation:  3
- Filters:          3 
- Scripting:        3
- Total:	    22.75
