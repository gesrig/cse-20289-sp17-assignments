Homework 07
===========
1. In node create I used malloc, and allocated a size of the struct node. Then I also allocated memory using strdup to copy the string onto the heap. Thus when using node delete, I first deallocated the string variable using free and then I deallocated from struct. With the recursive call, I continued to delete using this process until the temp variable, which is incrementing through the list, is NULL. 

2. In list create, I used malloc to allocate the size of a struct list. This is 20 bytes. Two pointers and a size_t variable. When I deleted the list, I called the node delete function with the recursive call to free all the list's node memory. Than I used free to delete the list memory allocated. 

3. List_qsort works by utilizing the internal qsort function. It first changes the list to an array using the list_to_array function. This returns a pointer to a pointer called temp. The qsort function runs on the array. Now I need to update my pointers by setting head and tail to the start and the end. I connect the nexts to their following attributes and set the final to NULL. Then I free the array that I created. The average time complexity is O(nlog(n)) and the worst case is O(n). The average space complexity is log(n).

4. list_reverse works by using recursion. First it changes the tail to the head pointer. Then for the head pointer, we run reverse. This funciton takes in the current and previous node, which in our case initially is the head pointer and the NULL pointer. It then continues to run this process moving the current and previous pointer down the list. Finallly, it returns the tail which will become the head. The average time complexity for this recursive process is O(n) this is also the worst case. Each time it is dependant on how long the list is. The space complexity is O(1) because it is not changing any memory allocation.

5. list_msort works by running the command to set the head pointer to the start. And then incrementing the tail to the end. The actual function works recursively. It works by splitting the data into left and right and runs the msort call on the left and right sections. This breaks it down into single elements and then merges them. This then merges all the single elements and sorts them as they go. The split function internally uses quick and slow pointers to find the middle of the list and break it by initiating the middle nodes' next to NULL. The merge works by incrementing through the two elements and coordinating the next pointers to the appropriate nodes.  The space complexity for merge sort is log(n) because of the recursion. The time complexity is nlogn which is the best possible for a sorting method.


ACTIVITY 2:



| NITEMS	| SORT		| TIME		| SPACE			|
|------		|------		|------		|------			|
| 10		| MERGE		| 0.000999  | 0.503906		|
| 10		| QUICK		| 0.001998  | 0.000999		|
| 100		| MERGE		| 0.000999  | 0.503906		|
| 100		| QUICK		| 0.000999  | 0.000999		|
| 1000		| MERGE		| 0.001998  | 0.558594		|
| 1000		| QUICK		| 0.001998  | 0.001998		|
| 10000		| MERGE		| 0.008998  | 1.105469		|
| 10000		| QUICK		| 0.008997  | 0.008997		|
| 100000	| MERGE		| 0.113982	| 6.601562		|
| 100000	| QUICK		| 0.103984	| 0.103983		|
| 1000000	| MERGE		| 1.794726	| 61.539062		|
| 1000000	| QUICK		| 1.488773	| 1.478774		|
| 10000000	| MERGE		| 24.300304	| 610.855469	|
| 10000000	| QUICK		| 18.696157	| 18.944120		|


1. The trend in these results are as the number of items get larger, quick sort still remains faster however it takes up less amount of memory. The Qucik sort space is wrong and I beleive it is in the way I am cutting it because on command line it takes more memory than merge sort. These results are surprising considering merge sort is supposed to have a better time complexity than qsort.

2. Theoretical complexity is not always correlating to real world performance. Complexities match scale but do not take in consideration little amounts of data as well as hardware limitations.  
