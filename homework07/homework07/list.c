/* list.c */

#include "list.h"

/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
struct list *	list_create() {
    struct list* lstptr = malloc(sizeof(struct list));
	
	lstptr->head = NULL;
	lstptr->tail = NULL;
	lstptr->size = 0;


	return lstptr;
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l	    List to deallocate.
 * @return  NULL pointer.
 */
struct list *	list_delete(struct list *l) {
    struct list *lt = l;
	lt->head = node_delete(lt->head, true);
	lt->tail = NULL;
	free(lt);
	return NULL;
}

/**
 * Push front.
 *
 * This adds a new node containing the given string to the front of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_front(struct list *l, char *s) {
    struct node * nodeptr = node_create(s, l->head);	
	nodeptr->next = l->head;
	l->head = nodeptr;
	l->size = l->size + 1;	
	if (l->size == 1) {
		l->tail = nodeptr;
	}
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_back(struct list *l, char *s) {
	struct list * listptr = l;
	struct node * nodeptr = node_create(s, NULL);
    listptr->size = listptr->size + 1;	
	if (listptr->size == 1) {
		listptr->head = nodeptr;
		listptr->tail = nodeptr;
	}
	listptr->tail->next = nodeptr;
	listptr->tail = nodeptr;
}

/**
 * Dump list to stream.
 *
 * This dumps out all the nodes in the list to the given stream.
 * @param   l	    List structure.
 * @param   stream  File stream.
 */
void		list_dump(struct list *l, FILE *stream){
	struct node* nptr = l->head;
	while (nptr != NULL) {
		node_dump(nptr, stream); 	
		nptr = nptr->next;
	}	 

}

/**
 * Convert list to array.
 *
 * This copies the pointers to nodes in the list to a newly allocate array that
 * the user must later deallocate.
 * @param   l	    List structure.
 * @return  Allocate array of pointers to nodes.
 */
struct node **	list_to_array(struct list *l) {
	int size = l->size;
	struct node ** arr = malloc(size * sizeof(struct  node *));   
    struct node * value = l->head;
		
	for (int it = 0; it < size ; it++) {
		arr[it] = value;
		value = value->next;	
	}	 
	
	return arr;
}

/**
 * Sort list using qsort.
 *
 * This sorts the list using the qsort function from the standard C library.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_qsort(struct list *l, node_compare f) {
	struct node ** temp = list_to_array(l);
	//struct node * first = temp[0];
	qsort(temp, l->size, sizeof(struct node *), f);
	l->head = temp[0];
	l->tail = temp[l->size-1];
	for (int it = 0; it< l->size-1; it++ ) {
		temp[it]->next = temp[it+1];
	}	
	l->tail->next = NULL;

	free(temp);

}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l	    List structure.
 */
void		list_reverse(struct list *l) {
	l->tail = l->head;
	l->head = reverse(l->head, NULL);	

}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
struct node*	reverse(struct node *curr, struct node *prev) {
 	struct node * tail = curr;
	if (curr->next) {
		tail = reverse(curr->next, curr);
	}
	curr->next = prev;
	return tail;
}

/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_msort(struct list *l, node_compare f) {
//	msort(l->head, f);
	//struct list * list = l;
	l->head = msort(l->head, f);


	while ( l->tail->next != NULL) {
		l->tail = l->tail->next;
	}	
}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	msort(struct node *head, node_compare f) {
    struct node * left;
	struct node * right;
	struct node * headptr;   
	if (head->next == NULL) {
		return head;	
	}
	else if (head == NULL) {
		return NULL;	
	}		
	
	split(head, &left, &right);
	left = msort(left, f);
	right = msort(right, f);
	headptr = merge(left, right, f);

	return headptr;
}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void		split(struct node *head, struct node **left, struct node **right) {
	struct node * fast = head;
	struct node * slow = head;
	struct node * tail = head;
	fast = fast->next->next;
	slow = slow->next;
	while (fast != NULL && fast->next != NULL) {
		fast = fast->next->next;
		slow = slow->next;
		tail = tail->next;
	}
	tail->next = NULL;
	*(left) = head;
	*(right) = slow;
	

}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	merge(struct node *left, struct node *right, node_compare f) {
 	struct node* lptr = left;
	struct node * rptr = right;
	struct node *iterator = left;
    
	if ( f(&lptr, &rptr) < 0) {
		iterator = lptr;
		lptr = lptr->next;
	}
	else   {
		iterator = rptr;
		rptr = rptr->next;
	}
	struct node* newhead = iterator;


	while (lptr && rptr) {
		if (f(&lptr, &rptr) < 0) {
			iterator->next  = lptr;
			lptr = lptr->next;
			iterator = iterator->next;	
		}		
		else   {
			iterator->next = rptr;
			rptr = rptr->next;
			iterator = iterator->next;
		}			
	}	 
	
	if (lptr == NULL) {
		iterator->next = rptr;
	}
	if (rptr == NULL) {
		iterator->next = lptr;
	}  	   
	
	return newhead;
}
