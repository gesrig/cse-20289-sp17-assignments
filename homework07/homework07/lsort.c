/* lsort.c */

#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "  -n   Numerical sort\n");
    fprintf(stderr, "  -q   Quick sort\n");
    exit(status);
}

void lsort(FILE *stream, bool numeric, bool quicksort) {
    char buffer[BUFSIZ];
    struct list * listmain = list_create();
    
    while (fgets(buffer,BUFSIZ,stream)) {
       list_push_back(listmain, buffer); 
    } 
    
    if (numeric) {   
        if (quicksort) {
            list_qsort(listmain, node_compare_number);
        }
        else {
            list_msort(listmain, node_compare_number);
        }
    }    
    else {
        if (quicksort) {
            list_qsort(listmain, node_compare_string);
        }
        else {
            list_msort(listmain, node_compare_string);
        }
    }

   struct node* ptr = listmain->head;
   while (ptr != NULL) {
       printf("%d\n" , ptr->number);
       ptr = ptr->next;
    } 
    list_delete(listmain);
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    int agrind = 1;
    PROGRAM_NAME = argv[0];
    bool SORTNUM = false;
    bool SORTQ = false;

    while (agrind < argc && strlen(argv[agrind]) > 1 && argv[agrind][0] == '-') {
        char *arg = argv[agrind++];
        switch (arg[1]) {
            case 'n':
                SORTNUM = true; 
                break;
            case 'q':
                SORTQ = true;
                break;
            case 'h':
                usage(0);
                break;
            default:
                usage(1);
                break;
        
        }
    }

    
    lsort(stdin, SORTNUM, SORTQ);




    /* Sort using list */

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
