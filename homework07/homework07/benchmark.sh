#!/bin/sh

echo "| NITEMS	| SORT		| TIME		| SPACE		|"
echo "|------		|------		|------		|------		|"



for num in  10 100 1000 10000 100000 1000000 10000000 
do
	sortnS=$(shuf -i1-100000000 -n $num | ./measure ./lsort -n 2>&1> /dev/null | cut -d ' ' -f 1)
	sortnMT=$(shuf -i1-100000000 -n $num | ./measure ./lsort -n 2>&1> /dev/null | cut -d ' ' -f 2)
	sortnM=$(echo $sortnMT | cut -d ' ' -f 2)
	sortqS=$(shuf -i1-100000000 -n $num | ./measure ./lsort -n -q 2>&1> /dev/null | cut -d ' ' -f 1)
	sortqMT=$(shuf -i1-100000000 -n $num | ./measure ./lsort -n -q 2>&1> /dev/null | cut -d ' ' -f 1) 
	sortqM=$(echo $sortqMT | cut -d ' ' -f 2)
	if [ $num -gt 10000 ] 
	then
		echo '| '$num'	| MERGE		| '$sortnS'	| '$sortnM'		|'
		echo '| '$num'	| QUICK		| '$sortqS'	| '$sortqM'		|'
	else
		echo '| '$num'		| MERGE		| '$sortnS'     | '$sortnM'		|'
		echo '| '$num'		| QUICK		| '$sortqS'     | '$sortqM'		|'
	fi

done

