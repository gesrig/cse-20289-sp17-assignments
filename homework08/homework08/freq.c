/* freq.c */

#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "    -f FORMAT        Output format (KEY, KEY_VALUE, VALUE, VALUE_KEY)\n");
    fprintf(stderr, "    -l LOAD_FACTOR   Load factor for hash table\n");
    exit(status);
}

void freq_stream(FILE *stream, double load_factor, DumpMode mode) {
   char buffer[BUFSIZ];
   Map * mptr = map_create(0, load_factor);
    char * wrd;
    while(fgets(buffer,BUFSIZ, stream)) {
        wrd = strtok(buffer, " \t\n");
        while (wrd != NULL) {
            Entry * tempsearch = map_search(mptr, wrd);
            if (tempsearch == NULL)
                map_insert(mptr, wrd , (Value)(int64_t) 1  , (Type)(NUMBER)); 
            else
                map_insert(mptr,wrd, (Value) (tempsearch->value.number + 1), (Type)(NUMBER));
            wrd = strtok(NULL, " \t\n");
        }
    }    
    map_dump(mptr, stdout, mode); 
    map_delete(mptr);
       
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    double lfactor = -1;
    DumpMode mode = VALUE_KEY;
    int agrind = 1;
   
    
    while (agrind < argc && strlen(argv[agrind]) > 1 && argv[agrind][0] == '-') {
        char *arg = argv[agrind++];
    //    printf("%s\n" , arg);
            switch (arg[1]) {
                case 'f': 
                    if (strcmp(argv[agrind], "KEY") == 0) 
                        mode = KEY;
                    else if (strcmp(argv[agrind], "KEY_VALUE") ==0)
                        mode = KEY_VALUE;
                    else if (strcmp(argv[agrind], "VALUE_KEY") == 0)
                        mode = VALUE_KEY;
                    else if ( strcmp(argv[agrind], "VALUE") == 0)
                        mode = VALUE;
                    else {
                        usage(0);
                    }
                    break;
                case 'l':
                    lfactor = strtod(argv[agrind],NULL);
                    break;
                case 'h':
                    usage(0);
                    break;
                default:
                    usage(1);
                    break;
             }  
             if (agrind < argc -1) {
                 agrind++;
            }
    }
    /* Compute frequencies of data from stdin */
    
    freq_stream(stdin, lfactor, mode);
    
    return EXIT_SUCCESS;



}



/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
