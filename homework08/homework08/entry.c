/* entry.c: map entry */

#include "map.h"

/**
 * Create entry structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   next            Reference to next entry.
 * @param   type            Entry's value's type.
 * @return  Allocate Entry structure.
 */
Entry *		entry_create(const char *key, const Value value, Entry *next, Type type) {
    Entry * entryptr = calloc(1,sizeof( Entry));
    char * tempstr = strdup(key);
    entryptr->key = tempstr;
    entryptr->type = type; 
    if (type == STRING) {
        char * tempval = strdup(value.string);
        entryptr->value.string = tempval;
    }
    else {
        entryptr->value.number = value.number;
    }     

    entryptr->next = next;
    return entryptr;
}

/**
 * Delete entry structure.
 * @param   e               Entry structure.
 * @param   recursive       Whether or not to delete remainder of entries.
 * return   NULL.
 */
Entry *		entry_delete(Entry *e, bool recursive) {
    if (recursive) {
        Entry * temp = e;
        while (temp != NULL) {
            Entry * x = temp->next;
            free(temp->key);
            if (temp->type == STRING) {
                free(temp->value.string);
            }
            free(temp);
            temp = x;
        }
    }
    else {
        free(e->key);
        if (e->type ==STRING) {
            free(e->value.string);
        }
        free(e);
    }
                                                                                        
    return NULL;
}

/**
 * Update entry's value.
 * @param   e               Entry structure.
 * @param   value           New value for entry.
 * @param   type            New value's type.
 */
void            entry_update(Entry *e, const Value value, Type type) {
    Entry * temp = e;
    if (temp->type == STRING) {
        free(temp->value.string);    
    }
    if (type == STRING) {
        char * strval = strdup(value.string);
        temp->value.string = strval;          
    } 
    else {
        temp->value.number = value.number;
    }
    temp->type = type;  

}

/**
 * Dump entry.
 * @param   e               Entry structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void            entry_dump(Entry *e, FILE *stream, const DumpMode mode) {
    
    switch(mode) {

        case KEY:
            fprintf(stream,"%s" , e->key); 
            break;

        case VALUE:
            if (e->type == STRING) 
                fprintf(stream, "%s" , e->value.string);
            else
                fprintf(stream, "%ld" , e->value.number);
            break;

        case KEY_VALUE:
            if (e->type == STRING) 
                fprintf(stream, "%s\t%s" ,e->key,  e->value.string);
            else
                fprintf(stream, "%s\t%ld" ,e->key, e->value.number);
            break;


        case VALUE_KEY:
            if (e->type == STRING) 
                fprintf(stream, "%s\t%s" , e->value.string, e->key);
            else
                fprintf(stream, "%ld\t%s" , e->value.number, e->key);
            break;
    }
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
