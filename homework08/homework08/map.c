/* map.c: separate chaining hash table */

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map *	        map_create(size_t capacity, double load_factor) {    
    Map * mapptr = calloc(1, sizeof(Map));
    if (capacity > 0) {
        mapptr->capacity = capacity;
    }
    else { 
        mapptr->capacity = DEFAULT_CAPACITY;
    }
         
    mapptr->size = 0;
    if (load_factor >0) {    
        mapptr->load_factor = load_factor;
    }
    else {
        mapptr->load_factor = DEFAULT_LOAD_FACTOR;
    }
    mapptr->buckets = calloc(mapptr->capacity, sizeof(Entry));
 

    return mapptr;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map *	        map_delete(Map *m) {
    
    for (size_t it = 0; it < m->capacity; it++) {
        Entry* temp = m->buckets[it].next;
        Entry * nullptr = entry_delete(temp, true);
    }
    free(m->buckets); 
    free(m);  
    return NULL;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void            map_insert(Map *m, const char *key, const Value value, Type type) {
    double ratio = ((double)m->size / (double)m->capacity) ;

    if (ratio >= m->load_factor) {
        map_resize(m, 2*(m->capacity));
    } 
    
    Entry * srptr = map_search(m, key);

    if (srptr == NULL) {
        uint64_t hashval = fnv_hash(key, strlen(key));
        hashval = hashval % m->capacity;
        Entry * ptr = m->buckets[hashval].next;
        /*
        while(ptr->next != NULL) {
            ptr = ptr->next;
        }*/
        m->buckets[hashval].next = entry_create(key, value, ptr, type);
       // ptr->next = entry_create(key, value, NULL, type);
        m->size++;        
    }
    else {
        entry_update(srptr, value, type);
    }

   
}

/**
 * Search map data structure by key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */
Entry *         map_search(Map *m, const char *key) {
    uint64_t hashval = fnv_hash(key, strlen(key));
    
    hashval = hashval % (m->capacity);

    Entry * searchptr = m->buckets[hashval].next;
    //printf("Hello we are at the while\n");
    while(searchptr != NULL && (strcmp(key, searchptr->key) !=0) ) {
        //printf("INSIDE TEH WHILE\n");
        searchptr = searchptr->next;
    }
        
    return searchptr;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool            map_remove(Map *m, const char *key) {
    Entry * findptr = map_search(m, key);

    if (findptr == NULL) {
        return false;
    }
    else {
        uint64_t hashval = fnv_hash(key, strlen(key));
        hashval = hashval % m->capacity;       
        Entry * sptr = m->buckets[hashval].next;
        if (strcmp(sptr->key, key) == 0) {
            m->buckets[hashval].next = sptr->next;
            entry_delete(sptr, false);
            m->size--;
            return true;
        } else {
            while (strcmp(sptr->next->key , key) != 0 ) {
                sptr = sptr->next;
            }
            Entry * t = sptr->next;
            sptr->next = t->next;
            entry_delete(t, false);
            m->size--;
            return true;
        }                     
    
    }
    
}

/**
 * Dump all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void		map_dump(Map *m, FILE *stream, const DumpMode mode) {
      
    for (size_t it = 0; it < m->capacity; it++) {
        Entry * temp = m->buckets[it].next;
        while (temp != NULL) {
            entry_dump(temp, stream, mode);
            printf("\n");
            temp = temp->next;
        }
    }
       

}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void            map_resize(Map *m, size_t new_capacity) {
    Entry * bucketptr = m->buckets;
    m->buckets = calloc(new_capacity, sizeof(Entry));
    
    size_t osize = m->capacity;

    for(size_t it = 0; it < osize ; it++) {
        Entry * eptr = bucketptr[it].next;
        while (eptr != NULL) {
            //map_insert(m, eptr->key, eptr->value, eptr->type);
            uint64_t hashval = fnv_hash(eptr->key, strlen(eptr->key));
            hashval = hashval % new_capacity;
            Entry * tempentry = eptr;
            eptr = eptr->next;
            Entry * secondtemp = m->buckets[hashval].next;
            m->buckets[hashval].next = tempentry;
            tempentry->next = secondtemp;
                         
        }           
    }
      
    m->capacity = new_capacity;   
    free(bucketptr); 
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
