Homework 08 - Grading
=====================

**Score**: 14.25/15

Deductions
----------
-.25 readme Act1 Q4: worst case time complexity is O(n), average case space complexity is O(1)
-.25 readme Act1 Q5: worst case time complexity is O(n), worst/average case space complexity is O(1)
-.25 readme Act1 Q6: worst case time complexity is O(n), worst/average case space complexity is O(1)

Comments
--------
nice work
