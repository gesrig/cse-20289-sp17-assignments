iHomework 08
===========
ACTIVITY 1:

1. In entry_create I calloced the size of the struct entry with only one 
of these memory chunks being created. Moreover, I created memory for the 
char * key value and then also if the type was STRING i used the same 
method to store the value. Thus when I deallocated the memory in delete, 
whether it was recursive or not, I had to deallocate the key value 
first, then if type was string the value memory. Lastly I could free the 
map memory. If the recursive flag was on, I continued to increment 
through the list of entries until it got to the NULL pointer.

2.  In mamp_create I calloced the size of the struct map and had one 
instance of it. Then, I also created memory using calloc of the size of 
an Entry and created the maps capacity of these entries. In map_delete, 
I iterated through the buckets and deleted the entries associated with 
each bucket using entry_delete with the recursion set to true. Then I 
freed the buckets I callocaed. Then I freed the map memory I allocated. 

3. In map_resize, I first allocated a new chunk of memory with calloc 
each with the size of Entry and an amount of them correlating to the 
new_capacity passed in to it. THen I run the hash functions on the 
previous bucket memory and put those entries into their new place. After 
this, I free the original bucket memory and make m->buckets point to the 
new chunk I created. This function is called whenever the ratio of size 
to capacity is greater than the Load factor. It grows the map, by making  
a new chunk of memory larger than that of the original. 

4. My map insert function works by first creating a ratio of size to 
capacity. To do this I had to typecase each value to double. Then I 
check if this is greater than or equal to my loadfactor. If it is, then 
I call my resize function. Then I call my search function. If the 
returned pointer isn't null, then I update the value with entry_update. 
If it is null, I do the same hash function and push front the value and 
rehook the next pointers to remain congruent. The big O for the insert 
function is 0(1) because of the ability for random access and pushing 
front of the list. The space complexity is O(n) because of the linked list. 

5. The search function works by calling the fnv hash function with the 
key and strlen(key). Then I mod this with the size capacity to find the 
bucket I want to insert this key into. I mark a pointer at this bucket's 
next value. I iterate this pointer until it is not null, or it doesn't 
find an identical key. If it gets to NULL, I return that, if not, I know 
it is a pointer to a key that already exists. The big O for time 
complexity for search is also O(1) becaause of this hash function that 
allows random access. 

6. My map_remove function works by calling the search function. If the 
search function returns a NULL pointer, as in the key doesn't have a 
value already, then it can't return and it fails. If not I do a similar 
hash function to find what bucket it is associated with. I set a pointer 
to this bucket.next entry. If this value is the key I look for, I call 
the entry_delete function, hook up the next pointers and decrement my 
size. If that isn't the key, I iterate until the pointers next value is 
the key. Then I do the same function to make sure my next pointers are 
correctly linked and then call the entry_delete function. The delete 
big O for time complexity is also O(1) because it uses the same search 
and then removes it. 

ACTIVITY 2:
1. Based on running the benchmark.sh, the overall trend I get in time and space is as the number of 
elements increases, the time also increases. Moreover, the time also increases in corrrelation with the 
alpha or load_factor. For larger numbers, the space increases. However as we increase the alpha number, 
the space used actually has an inverse relationship and decreases. The results are not, because as the 
tolerance before sizing gets larger, than there is less resizing and less memory in use. 

2. The advantages of the Hash table is that you can iterate through it in a linear fashion and also 
finding the hash bucket is of O(1). That being said, this hash table also allocates memory to store the 
amount of buckets needed as well as any list associated with this bucket. A treap is a BST which means 
when searching for a node it is of O(log(n)). However, you can not do random access to find the key. The 
treap also has a balancing mechanism within it to keep it as regulated as possible. If I need to use on 
as the underlying data structure for a map, it would depend on how large the map would need to be and how 
many elements there are. A binary Search tree is more efficient in terms of memory. That being said, a hash table is quicker. Thus for less large data sets it will be much quicker. Also, a tree is sorted which poses other constraints. The data struture must be used to fit the needs of its environment.

 | NITEMS	| ALPHA		| TIME		| SPACE			|
 | ------	| -----		| ----		| -----			|
 |10		| 0.5		| 0.000999	| 0.558594		|
 | 10		| 0.75		| 0.000999	| 0.554688		|
 | 10		| 0.9		| 0.000999	| 0.558594		|
 | 10		| 1.0		| 0.000999	| 0.558594		|
 | 10		| 2.0		| 0.000999	| 0.558594		|
 | 10		| 4.0		| 0.000999	| 0.558594		|	
 | 10		| 8.0		| 0.000999	| 0.554688		|
 | 10		| 16.0		| 0.000999	| 0.558594		|
 | 100		| 0.5		| 0.001998	| 0.566406		|
 | 100		| 0.75		| 0.000999	| 0.562500		|
 | 100		| 0.9		| 0.000999	| 0.566406		|
 | 100		| 1.0		| 0.001998	| 0.562500		|
 | 100		| 2.0		| 0.000999	| 0.566406		|
 | 100		| 4.0		| 0.000999	| 0.566406		|
 | 100		| 8.0		| 0.000999	| 0.562500		|
 | 100		| 16.0		| 0.000999	| 0.566406		|
 | 1000		| 0.5		| 0.001999	| 0.664062		|
 | 1000		| 0.75		| 0.001999	| 0.679688		|
 | 1000		| 0.9		| 0.001999	| 0.691406		|
 | 1000		| 1.0		| 0.001999	| 0.632812		|
 | 1000		| 2.0		| 0.001998	| 0.628906		|
 | 1000		| 4.0		| 0.001998	| 0.632812		|
 | 1000		| 8.0		| 0.001998	| 0.632812		|
 | 1000		| 16.0		| 0.001998	| 0.628906		|
 | 10000	| 0.5		| 0.015997	| 2.609375		|
 | 10000	| 0.75		| 0.013997	| 1.792969		|
 | 10000	| 0.9		| 0.013997	| 1.785156		|
 | 10000	| 1.0		| 0.014997	| 1.855469		|
 | 10000	| 2.0		| 0.013997	| 1.542969		|
 | 10000	| 4.0		| 0.013997	| 1.410156		|
 | 10000	| 8.0		| 0.013997	| 1.347656		|
 | 10000	| 16.0		| 0.012997	| 1.320312		|
 | 100000	| 0.5		| 0.192970	| 17.476562		|
 | 100000	| 0.75		| 0.213967	| 19.980469		|
 | 100000	| 0.9		| 0.172973	| 12.148438		|
 | 100000	| 1.0		| 0.176972	| 12.160156		|
 | 100000	| 2.0		| 0.155975	| 10.156250		|
 | 100000	| 4.0		| 0.159974	| 9.160156		|
 | 100000	| 8.0		| 0.175972	| 8.652344		|
 | 100000	| 16.0		| 0.211967	| 8.410156		|
 | 1000000	| 0.5		| 2.026691	| 140.820312	|
 | 1000000	| 0.75		| 2.328645	| 156.480469	|
 | 1000000	| 0.9		| 2.487621	| 168.488281	|
 | 1000000	| 1.0		| 1.796726	| 108.824219	|
 | 1000000	| 2.0		| 2.071685	| 92.824219		|
 | 1000000	| 4.0		| 2.141673	| 84.816406		|
 | 1000000	| 8.0		| 2.616601	| 80.824219		|
 | 1000000	| 16.0		| 3.700436	| 78.820312		|
 | 10000000	| 0.5		| 25.219166	| 2176.472656	|
 | 10000000	| 0.75		| 22.233620	| 1275.464844	|
 | 10000000	| 0.9		| 23.807381	| 1344.484375	|
 | 10000000	| 1.0		| 24.059341	| 1408.472656	|
 | 10000000	| 2.0		| 24.999199	| 1024.480469	|
 | 10000000	| 4.0		| 28.590652	| 891.468750	|
 | 10000000	| 8.0		| 34.349777	| 827.468750	|
 | 10000000	| 16.0		| 49.700443	| 795.460938	| 
