#!/bin/sh

echo "| NITEMS	| ALPHA		| TIME		| SPACE		|"
echo "| ------	| -----		| ----		| -----		|"



for num in  10 100 1000 10000 100000 1000000 10000000 
	do
		for alpha in 0.5 0.75 0.9 1.0 2.0 4.0 8.0 16.0
			do
			freqnS=$(shuf -i1-100000000 -n $num | ./measure ./freq -l $alpha 2>&1> /dev/null | cut -d ' ' -f 1)
			freqMT=$(shuf -i1-100000000 -n $num | ./measure ./freq -l $alpha 2>&1> /dev/null | cut -d ' ' -f 2)
			freqM=$(echo $freqMT | cut -d ' ' -f 2)
		
	
			if [ $num -gt 10000 ]
			then
				echo '| '$num'	| '$alpha'		| '$freqnS'	| '$freqM'	|'
			else
				echo '| '$num'		| '$alpha'		| '$freqnS'	| '$freqM'	|'

			fi
			done

done
