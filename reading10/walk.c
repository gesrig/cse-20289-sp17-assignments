//Gregory Esrig. Walk.c
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
int main() {
	char cwd[1024];
	struct stat filestat;
	getcwd(cwd, sizeof(cwd)); 
	DIR *dir;
	struct dirent *filent;

	if ((dir = opendir(cwd)) !=NULL) {
		//printf("YAY");
		while ((filent = readdir(dir)) != NULL) {
			stat(filent->d_name, & filestat);
			if (S_ISREG(filestat.st_mode)) {
				printf("%s ", filent->d_name);
				printf("%jd\n", filestat.st_size);
			}
		}
		closedir(dir);
	}
	else {
		perror("");
	//	return EXIT_FAILURE;
	}

}

