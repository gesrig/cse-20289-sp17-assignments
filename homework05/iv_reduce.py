#!/usr/bin/env python2.7
import sys

counts = {}
 
for line in sys.stdin:
    k, v  = line.strip().split('\t', 1)
    counts.setdefault(k, []).append(int(v))
    

    
for key, value in counts.iteritems():
    temp  = sorted(set(value))
    counts[key] = temp
   
for k, v in sorted(counts.items()):
     print '{}\t{}'.format(k, ' '.join(map(str, v)))
      
