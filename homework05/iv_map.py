#!/usr/bin/env python2.7

import sys
PUNC = '-abcdefghijklmnopqrstuvwxyz'
counter = 0
for line in sys.stdin:
    counter = counter + 1
    for word in line.strip().split():
       word = word.lower()
       for letter in word:
           if letter not in PUNC:
		word = word.replace(letter, "")
		
       print '{}\t{}'.format(word, counter)

