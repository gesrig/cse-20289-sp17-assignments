Reading 01
==========
Gregory Esrig (gesrig)

1.
a.  What is the purpose of the | operator?
	The purpose of the | operator is to allow the standard output of one command to be piped into the standard input of another. Thus in this case it is taking the command of du -h /etc/ 2>  /dev/null and piping it into the command of sort 

b. what is the purpose of the 2> /dev/null
	This is outputting standard error into the null file. This is for unwanted output. /dev/null is a bit bucket and does nothing with what is sent there.

c. what is the purpose of the > output.txt?
	This takes the standard output of the previous commands and stores it into the text file called output.txt

d. what is the purpose fo the -h flags?
	The -h flag puts the file size  in human readable form rather than bytes

e. Is the following command the same as above?
	The following command is not the same. You'll experience the ouput to the screen. 
2. 
a. concatenate all files from 2002 into one file
	cat 2002*  > 2002files.txt
b. all files from month of december
	cat *[12] > Decfiles.txt
c. view contents of all files from month of january to june?
	cat *{01..06}

d. view contents from files with even year and odd month
	cat {2002,2004,2006}-{01,03,05,07,09,11}
e. contents of files from 2002 - 2004 months 09-12
	cat {2002..2004}-{09,10,11,12}

3.
a. which files are executable by the owner?
	The second and third
b. Which files are readable by members of the users group?
	Second and the third
c.  which files are writable by anyone
	none of them
d. What command would you execute so that Tux had the same permissions as Huxley?
	chmod 755 Tux
e. what command so Tux had same permission as Beastie
	chmod 600 Tux

4. 
a. suspend the process
	cntrl -z
b. resume the suspended process
	fg 
c. Signal end of process
	cntrl d
d. terminate the bc process
	cntrl c
e. terminate process from another terminal
	kill -TERM
