Reading 03
==========
1. echo "monkeys love bananas" | tr '[a-z]' '[A-Z]'
Using the tr command, this takes an value in the set from lowercase a to z and changes it to uppercase A-Z. Thus transforming all letters to uppercase

2. echo "monkeys love bananas" | sed 's/monkeys/gorillaz/'
	using the sed command, this takes the instance of monkeys and changes it to gorillaz. If there were more than one you would need to put a g at the end

3. echo "     monkeys love bananas" | sed "s/^[ \t]*//"
	In this, the sed command uses the /t and * to take away any whitespace preceding the actual text. 

4. 
	$ cat /etc/passwd | grep ^"root" | cut -d ':' -f 7
	In this instance. It parses through to find the instance of grep. Because it is on the seventh deliminator with :, we are going to cut everything but that

5.
	$ cat /etc/passwd | sed 's/\/bin\/csh/\/usr\/bin\/python/g' | grep python ; cat /etc/passwd | sed 's/\/bin\/tcsh/\/usr\/bin\/python/g' | grep python ; cat /etc/passwd | sed 's/\/bin\/bash/\/usr\/bin\/python/g' | grep python	       

6.

	$ cat /etc/passwd | grep -e '4[0-9]+7|47'
	This searches for all numbers that start with 4, then have numbers through 0-9 and then end in 7. 


7.
	$ comm -12 file1.txt file2.txt
	Description: This supresses the columns one and two which in turn shows the similarities

8.
	$ comm -3 file1.txt file2.txt
	Description: This supresses the third column which shows the similarities
