#!/usr/bin/env python2.7

import os
import requests
import re
import sys

#defualt values
FIELD = 'title'
LIMIT = 10
SUBREDDIT = 'linux'
URL = 'https://www.reddit.com/r/'
REGEX = ''
def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -f  FIELD        what Field you want to search, default 'title'
    -n LIMIT   number of items displayed on the screen (default: {})
    -s SUBREDDIT The SUBREDDIT you want to search (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)


args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    # TODO: Parse command line arguments
    if arg == '-f':
        FIELD = args.pop(0)
    elif arg == '-n':
        LIMIT = int(args.pop(0))
    elif arg == '-s':
        SUBREDDIT = args.pop(0)
    else:
       usage(1)

if len(args):
     REGEX = args.pop(0)

headers  = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
response = requests.get(URL + SUBREDDIT + '/.json' , headers=headers)
REDdir = response.json()


#if FIELD in REDdir:
if FIELD == 'title' or FIELD == 'author' or FIELD == 'link' or FIELD == 'short':
    if response.status_code == 200:
        for index, child in enumerate(REDdir['data']['children']):
            if (index < LIMIT) :
                if re.search(REGEX, child.get('data').get(FIELD)):
                     print  '{}\tTitle:\t'.format(index+1) + child.get('data').get('title')+''
                     print '\tAuthor:\t' + child.get('data').get('author')
                     print '\tLink:\t' + URL + child.get('data').get('permalink')

    else:
        print 'THIS URL IS INVALID '
        sys.exit(1)
else:
    print 'THIS FIELD IS INVALID: ' + FIELD
    sys.exit(1)
