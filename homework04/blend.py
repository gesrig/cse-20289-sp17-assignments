#!/usr/bin/env python2.7

import atexit
import os
import re
import shutil
import sys
import tempfile

import requests

# Global variables
URL	    = 'https://engineering.nd.edu/profiles/'
REVERSE     = False
DELAY       = 20
STEPSIZE    = 5
os.environ['PATH'] = '~ccl/software/external/imagemagick/bin:' + os.environ['PATH']
# Functions
def cleanup(name):
	shutil.rmtree(name)
	sys,exit(0)

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    # TODO: Parse command line arguments
    if arg == '-r':
	REVERSE = True
    elif arg == '-d':
	DELAY = int(args.pop(0))
    elif arg == '-s':
	STEPSIZE = int(args.pop(0))
    else:
	usage(1)
 
if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution

# TODO: Create workspace
PATHNAME = tempfile.mkdtemp()
# TODO: Register cleanup

# TODO: Extract portrait URLs
PortraitRes1 = requests.get(URL + netid1)
PortraitRes2 = requests.get(URL + netid2)

#exit early if invalid URL
if PortraitRes1.status_code != 200 or PortraitRes2.status_code != 200:
     print 'wrong NETID'
     sys.exit(1)

PortraitURL1 = re.findall(r""+URL + netid1+".*jpeg", PortraitRes1.text)
PortraitURL2 = re.findall(r""+URL + netid2+".*jpeg", PortraitRes2.text)
# TODO: Download portraits

jfile1 = requests.get(PortraitURL1[0])
jfile2 = requests.get(PortraitURL2[0])

image1 = os.path.join(PATHNAME, 'netid1.jpg')
image2 = os.path.join(PATHNAME, 'netid2.jpg')

with open(image1, 'w') as fs:
     fs.write(jfile1.content)
with open(image2, 'w') as fs:
     fs.write(jfile2.content)

# TODO: Generate blended composite images

for increment in range(0,101,STEPSIZE):
     runthis = 'composite -blend {} {} {} {}/out{}.gif'.format(increment,image1,image2, PATHNAME, increment)
     os.system(runthis)



# TODO: Generate final animation

if REVERSE == False:
     print "not reversed"
     varstring = 'convert -loop 0 -delay {} '.format(DELAY)
     for increment in range(0,101,STEPSIZE):
	varstring += '{}/out{}.gif{}'.format(PATHNAME,increment,' ')
     
     os.system(varstring + target) 
else:
     print "reversed"
     varstring = 'convert -loop 0 -delay {} '.format(DELAY)
     for increment in range(0,101,STEPSIZE):
	varstring += '{}/out{}.gif{}'.format(PATHNAME,increment,' ')
     for increment in range(100, 0,-STEPSIZE):
	varstring += '{}/out{}.gif{}'.format(PATHNAME,increment,' ')
     
     os.system(varstring + target) 


atexit.register(cleanup(PATHNAME)) 
