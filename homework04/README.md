Homework 04
===========
ACTIVITY 1.
1. I parsed the command line arguments with a while loop similar to the one in the reading assignment this past week. The loop runs continuously until the commands don't start with a - and it's greater than 0. Using the pop function, which deletes and returns the value, I took the command from the args string (the list from sys.argv). Then using a if , elif statement, I specified what the symbols designated and assigned the next value.

2. I managed the temporary workspace by using the function tempfile.mkdtemp(). This creates the pathname to the temp directory (absolute path name). I was then able to store certain images in and strings in here by calling the variabnle PATHNAME (the absolute path). Eventually when I concatenated the string, I would use this variable to manage the workspace.

3. I extracted the Portrait URLs by using the requests.get and findall commands. First, I took the url + netid string to essentially get the contents of the HTML page. Using findall, I parsed through it with regular expressions. THese looked for any code that ended in jpeg. This returns a list with everything, but since there was only one, I would use the first element in the next step.

4. I downloaded the images using the requests.get function again except on the first element in the list of the findall command. This stored the contents into a new variable I would call jfile. I then used the open command to write these contents into my temporary workspace.

5. I then blended the images by first using the composite function. To generate this command, I used the format function to add in my increment (going from 0 to 100), my image1, image2 (both the subspace in the temp directory), the path to the temporary directory, and again my increment.

6. I generated the final animation by creating a string of all the different arguments and images. First, I checked if it would run forward and backward with a simple if statement checking if the -r flag was on the command line. Next, I kept adding arguments to the variable var string that were separated by one space. I then ran the os.system to perfrom the convert command on all the other things that came after.

7. Many times if there is an invalid netid or URL, the webpage will return a 404 error. To check for this, I put in after downloading the images (before moving on) if the resource code was != 200 (200 signifies it exists). If either URL returned this I printed out an error message and used sys.exit(1) to leave.



ACTIVITY 2:

1. I parsed the command line arguments in almost an identical way to the previous activity. Using a while loop until the commands don’t begin with dashes anymore. However, at the end I also included a command seeing if the length of the list args wasn’t empty. If it wasn’t, and there still was a regex to be taken in, I would make that my variable REGEX.

2.
I fetched the JSON data by using the commands given to trick Reddit. After declaring headers, I used the requests.get function to get the URL with the corresponding SUBREDDIT. From here, I used the .json() extension to write it into a dictionary stored into the variable called REDdir.

3. I filtered the article based on the field and REGEX by using the re.search function. This takes in two inputs, the first is the regular expression to be searched. If none is specified it is just a blank. However if there is one REGEX variable is set to this. The second argument in the search function is child.get(‘data).get(FIELD). This signifies it is searching the field that was specified. Default value is set to title

4. This was omitted from the project specifications
