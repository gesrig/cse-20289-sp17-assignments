/* stringutils.c: String Utilities */

#include "stringutils.h"

#include <ctype.h>
#include <string.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *  string_lowercase(char *s) {
    for (char *c =s; *c; c++) {
        *c = tolower(*c);
        
    }

    return s;
}


/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_uppercase(char *s) {
    for (char *c =s; *c; c++)
        *c = toupper(*c);

     return s;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool	string_startswith(char *s, char *t) {
     
    for (t ; *t != '\0'; t++, s++) {
        if ( *t != *s ) {
            return false;
        }
    }
    return true;
        
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool	string_endswith(char *s, char *t) {
    s = s + strlen(s) - strlen(t);    
    for (t ; *t != '\0'; t++, s++) {
        if ( *t != *s ) {
            return false;
        }
    }
    return true;

}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_chomp(char *s) {
    char * temp = s;
    temp = s + strlen(s) -1;
    if ( *temp == '\n') {
        *temp = '\0';
    }
    
    
    return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_strip(char *s) {
    
    char *r = s;
    char *w = s;

    while (*r == ' ') {
        r++;
    }

    while ( *r != '\0'){
        *w = *r;
        w++;
        r++;

    }   

    *w = *r;
    r = s + strlen(s) -1;

    while ( *r == ' ') {
        r-- ;
        *(r+1) = '\0';
    }

    
    return s;
}

/**
 * Reverses a string given the provided from and to pointers.
 * @param   from    Beginning of string
 * @param   to      End of string
 * @return          Pointer to beginning of modified string
 **/
static char *	string_reverse_range(char *from, char *to) {

    char * f = from;
    char * t = to;
    int counter =0;
    while ( f != t ) {
        counter ++;
        f++;
    }
    f = from;
    t = to;;
    int move = counter/2;

    for ( int it = 0; it <=move; it++) {
        char temp = *(t-it);
        *(t-it) = *(f+it);
        *(f+it) = temp;
    
    }

    return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse(char *s) {
    int size = strlen(s);
    if (size ==0) {
        return s;
    } 
    char * from = s;
    char * to = s + size -1;

    s = string_reverse_range(from, to);

    return s;
}

/**
 * Reverses all words in a string.
 * @param   s       String with words to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse_words(char *s) {
  int size = strlen(s);
    if (size ==0) {
        return s;
    }
    char * from = s;
    char * to = s + size -1;
    char *string = string_reverse_range(from, to);
    from = string;
                            
    while (*to != '\0') {
        while (*from == ' ') {
            from++;
        }
        if (*from == '\0') {
            break;
        }
        to = from;
        while ((*(to+1) != ' ' ) && (*(to+1) != '\0')) {
            to ++;
        }
        string = string_reverse_range(from, to);
        from = ++to;
    }
                                                                                                              

    return s;
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char *	string_translate(char *s, char *from, char *to) {
    int sizef = strlen(from);
    int sizet = strlen(to);
    if (sizef != sizet) {
        return s;
    }    


    char * t = to;

    for (char *start =s; *start; start++) {
        for (char *f = from; *f; f++) {
            if (*start == *f) {
                *start = *t;
            }
            t++;
        }
        t = to;

    }
    
    
    return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base) {
    int power = base;
    base = 1;
    int size = strlen(s);
    int total = 0;
    int counter = 0;
    int intval = 0;
    char * digit = s + size -1; 
while ((*digit >= '0' && *digit <= '9') || (*digit >= 'A' && *digit <= 'F') || (*digit >= 'a' && *digit <= 'f')) {
    if (*digit >= '0' && *digit <= '9') {
        intval = (*digit - '0');
        total = total +intval*base;;
        digit--;
        counter++;
    }
    else if  (*digit >= 'A' && *digit <= 'F') {
        intval = (*digit - 55);
        total = total +intval*base;
        digit--;
        counter++;
    }
    else if  (*digit >= 'a' && *digit <= 'f') {
        intval = (*digit - 87);
        total = total +intval*base;
        digit--;
        counter++;
    }
    base = base*power;
}




    
    return total;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
