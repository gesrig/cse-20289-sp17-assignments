iHomework 06
===========
ACTIVITY 1:

1. My string_reverse_words function works by first reversing the string 
itself. Then I assign a pointer the front and move until it gets the the 
first letter. I assign a next pointer equal to this and move it out to 
the end of the word. I call reverse range on this function and continue 
to do this process until one of the pointers equals a null block of 
memory. The time complexity of this function is O(n) because it is 
completely dependant on the size of the initial string that it parses as 
the pointer moves across it incrementally. The space complexity is O(1) 
because there will always be memory for two pointers and its fixed 
string length that is altered. 

2. My string translate works by first checking to see if it is a valid 
source and target set by checking the size of each. Then it assigns a 
pointer to the start of the s string and loops through it, and each time 
loops through the source to see if there are any matches. If there are 
matches, it replaces the value with the pointer's value at the target 
set. The time complexity of this function is O(n) because of the double 
for loop. The space complexity of this function is still O(1) because 
the string memories and pointer memories are constant and are not 
altered as it gets scaled.

3. My string to integer function works by running through the string 
given and continues incrementing until the value of the pointer is not a 
digit or letter between a and f. If it is between one of these numbers, 
then it adds the total by multiplying the integer value of the digit 
(*digit - '0') by the base. Then the base number gets multiplied by 
itself to simulate a power function. Then, the total value is returned. 
The time complexity of this function is O(n) because it is completely 
dependant on how many digits there are and is scaled accordingly to that 
size. The space complexity is O(1) due to the set memory occuring when I 
parse the digits. The total variable which stores the value will be the 
only addition as well as the counter.  

4. The difference between a shared library and a static library is that 
a .so file has all code relating to the library in this file and it is 
referenced at run-time. Programs using the shared libaries only make 
reference to the code it uses in the shared library. Static libraries 
are directly linked into the program at compile time. Shared libraries 
reduce the amount of code duplicated in each program. Static libraries 
increase the overall size fo the binary but it also makes it so you 
don't need to carry along a copy of the library being used. The shared 
library is 12K and the shared library is 14K. The shared library.


ACTIVITY 2:

1. The translate.c program has many steps. First to parse the command 
line arguments, it works by taking in values as long as there is a - out 
front. If there is, it increments through the argv array and implements 
a switch statement to update mode's value. The mode bitmask values were 
enumerated in a separate function, shifting over the value of one bit 
each time (1<<1, 1<<2) and so on. When a certain case was selected, it 
would assign the mode bitmask to this value. To handle the 
translate_stream used fgets to read the command line arguments into an 
array called buffer. A new pointer was assigned to this value and these 
values were continually updated based on what modes were selected 
previously. Based on the if statements, certain functions were invoked 
on the buffer array values. These would only be called if the 
corresponding mode was previously selected.


2. The difference between a static executable and a dynamic executable 
is a static executable will hold all required binary code inside it. A 
dynamically linked executable will look for the libraries it's linked 
against at runtime and thus will hold much less memory. This is the 
reason why translate-static is 885K vs just 14K in the 
translate-dynamic. When trying to execute translate-static it did work. 
For example, this executable is how the homework executed certin 
commands such as echo "    Hello World" | ./translate-static -u. This is 
not the same case for the dynamic executable. To make this an executable 
we need to make sure the shared library is found. To do this we 
designate the LD_LIBRARY_PATH so the code knows where to find the.so 
file.
