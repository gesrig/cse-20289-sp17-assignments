/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    STRIP = (1<<1),
    REVERSE =( 1<<2),
    REVWORDS = (1<<3),
    CONLOW = (1<<4),
    CONUP = (1<<5),

};    


/* Globals */

char *PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s     Strip whitespace\n");
    fprintf(stderr, "   -r     Reverse line\n");
    fprintf(stderr, "   -w     Reverse words in line\n");
    fprintf(stderr, "   -l     Convert to lowercase\n");
    fprintf(stderr, "   -u     Convert to uppercase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode) {
    char buffer[BUFSIZ]; 
    char *text = &buffer;
    //text = string_chomp(text);
    while (fgets(buffer,BUFSIZ,stream)) {
        //based on the function implement a certain one 
        if (mode & STRIP) {
            text = string_chomp(text);
            text = string_strip(text);
        }
        if (mode & REVERSE) {
            text = string_chomp(text);
            text = string_reverse(text);;
        }
        if (mode & REVWORDS) {
            // fputs(string_reverse_words(buffer), stdout);
            text = string_chomp(text);
            text = string_reverse_words(text);
        } 
        if (mode & CONLOW) {
            text = string_chomp(text);
            text = string_lowercase(text);
        }
        if (mode & CONUP) {
            text = string_chomp(text);
            text = string_uppercase(text);
            source = string_uppercase(source);
        }
        if( strlen(source) >= 1) {
            text = string_chomp(text);
            text = string_translate(text, source, target);
        }     
        text = string_chomp(text); 
        printf("%s\n", text);
    }

}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */

    char * SOURCE = "";
    char * TARGET = "";
    int agrind = 1;
    int mode;
    mode = 0; 
    PROGRAM_NAME = argv[0];
    while (agrind < argc && strlen(argv[agrind]) > 1 && argv[agrind][0] == '-') {
        char *arg = argv[agrind++];
        switch (arg[1]) {
            case 's':
                // strip whitespace
                mode |= STRIP;
                break;
            case 'r':
                mode |=REVERSE; 
                //reverse line
                break;
            case 'w':
                mode |= REVWORDS;
                // reverse words
                break;
            case 'l':
                mode |= CONLOW;
                //convert to lowercase
                break;
            case 'u':
                mode |= CONUP;
                //convert to uppercase
                break;
            case 'h':
                usage(0);
                break;
            default:
                usage(1);
                break;
        }
    }

   // printf("HELLO\n");
    if ((agrind+2) == argc) {
        SOURCE = argv[agrind ];
        TARGET = argv[agrind + 1];
    }
//    printf("!!!!!\n"); 
    

    
    translate_stream(stdin, SOURCE, TARGET, mode);
    
    /* Translate Stream */

    return EXIT_SUCCESS;}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
