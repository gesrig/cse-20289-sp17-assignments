#!/bin/sh

if [ $# == 0 ]; then
	echo "Usage: extract.sh archive1 archive2..."
	exit 1
fi

for file in $@
do
	case $file in

		*tar.gz | *tgz)  tar -zxvf $file   ;;
		
		*tar.bz2 | *tbz) tar -xvjf   $file;;

		*tar.xz | *txz) tar -xvJf  $file;;

		*zip | *jar) unzip $file  ;;
	
	esac

done

exit 0
		
