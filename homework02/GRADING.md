Homework 02 - Grading
====================

**Score**: 14/15

Deductions
----------
extract.sh
-.25 doesnt handle the case of unsupported archive format

fortune.sh
-.25 doesnt display random message.
-.5 Some tests fail. Signals HUP, INT, and TERM dont exit properly

Comments
--------
For fortune.sh you didnt use shuf the way you talked about in the readme! Otherwise awesome job!
