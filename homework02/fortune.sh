cat > sayings << EOF
It is certain
It is decidedly so
Without a doubt
Yes, definitely
You may rely on it
As I see it, yes
Most likely
Outlook good
Yes
Signs point to yes
Reply hazy try again
Ask again later
Better not tell you now
Cannot predict now
Concentrate and ask again
Don't count on it
My reply is no
My sources say no
Outlook not so good
Very doubtful
EOF

export PATH=$PATH:/afs/nd.edu/user15/pbui/pub/bin

trap responses 1 2 3 15

responses()
{
        cowsay "I'm so wise. Why don't you want to ask me any more things"
}

cowsay Hey User What do you want to ask me?

echo Question?
read UserQuestion

while [[ -z "$UserQuestion" ]]
do 
	echo Question?
	read UserQuestion

done



cat sayings | shuf | head -n 1 | cowsay


