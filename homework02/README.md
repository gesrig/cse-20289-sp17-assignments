Homework 02
===========
EXTRACT:

1. I checked having no arguments by checking if  $# was 0. In other words only the executable was sent. If so I would put out the usage message

2. I determined which command to use through extensive research on StackOverflowas well as tutorials on what flags were meant to go with what. After deciphering the differences I came up with the answer to have the various tar commands

3. The most challenging aspect of writing this script was not the structure or conceptuals. It was mostly trying to figure out why some flags worked and what flags those were. I overcame this issue by googling, reading, and trying a variety of commands.

FORTUNE: 
1. I displayed random messages using a here doccument. Then by creating a pipeline command saying "cat sayings | shuf | head -n 1| cowsay" I was able to shuffle them, choose the first, and submit that to cowsay all in one argument

2. I handled signals using a trap case for numbers 1 2 3 15 whcih all correlate to various signals. This was put into a functinon called responses that submitted a certain message whenever one of these signals was called.

3. I read input from the user usning a while loop. It waited for a question, and if there was none kept outputing 'question?.' The read command took in their variable. 


4. The most challenging aspect of writing this script was figuring out the order of commandments. My signals didn't work at first due to the fact that they were below my read command. Moreover, trying to find the correct syntax to pipe the shuffled here document was rather difficult. Again some research and collaboration with peers helped me realize the proper pipeline. 

ORACLE:

1. My first step was to scan `xavier.h4x0r.space` for a HTTP port:
	$ nc xavier.h4x0r.space -z 9000-9999
	Ouput: 
Connection to xavier.h4x0r.space 9097 port [tcp/*] succeeded!
Connection to xavier.h4x0r.space 9111 port [tcp/*] succeeded!
Connection to xavier.h4x0r.space 9876 port [tcp/sd] succeeded!

2. Next I tried to acces the HTTP server:
	$ curl xavier.h4x0r.space:9876

This outputted a message from a penguin giving me instructions to search the lockbox folder. And then request the doorman with the passcode I found. 


3. I then changed my directory to be in this one which was filled with a lot of giberish. I needed to find my folder so I inputted on the command line:

$ find -name "gesrig*" 

     Output: This then gave me the directory my folders were stored in and I then changed the directory into 'a257d4a4' 

4.

    Here I had to brute force the file with the .lockbox extension. Using strin commands

    $ for String in $(strings /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/5e784d55/41216aa0/615aa4f3/a257d4a3/gesrig.lockbox ) ; do /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/5e784d55/41216aa0/615aa4f3/a257d4a3/gesrig.lockbox $String ; done

	Output: 30931012af50aa386fea63aa6254e0a6. This is my password

5.
	Next I Needed to go into the other open port with my passcode in accordance with the directions from the graphics from the first port.

	$ curl xavier.h4x0r.space:9876/gesrig/30931012af50aa386fea63aa6254e0a6
       
        / Ah yes, gesrig... I've been waiting for \
| you.                                    |
|                                         |
| The ORACLE looks forward to talking to  |
| you, but you must authenticate yourself |
| with our agent, BOBBIT, who will give   |
| you a message for the ORACLE.           |
|                                         |
| He can be found hidden in plain sight   |
| on Slack. Simply send him a direct      |
| message in the form "!verify netid      |
| passcode". Be sure to use your NETID    |
| and the PASSCODE you retrieved from the |
| LOCKBOX.                                |
|                                         |
| Once you have the message from BOBBIT,  |
| proceed to port 9111 and deliver the    |
| message to the ORACLE.                  |
|                                         |
| Hurry! The ORACLE is wise, but she is   |
\ not patient!


6. 
    I then went into slack and sent bobbit a message 
    Command: verify gesrig 30931012af50aa386fea63aa6254e0a6

    Bobbit responded with a message for the oracle. @gesrig: Hi gesrig! Please tell the ORACLE the following MESSAGE: dHJmZXZ0PTE0ODYxNDUwNDM=


7.
    I then went back to the terminal where I would need to give this message to the oracle using a telnet command

    Command: $ telnet xavier.h4x0r.space 9111

8. 
    Here the Oracle asked for my name. My message which I got from slack. And a reason I took so long to get to the Oracle. Bobbit then congratulated me and hoped I learned soomething. The rest of the message was just random characters 
